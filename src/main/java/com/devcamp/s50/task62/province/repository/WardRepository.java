package com.devcamp.s50.task62.province.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task62.province.models.Ward;

public interface WardRepository extends JpaRepository<Ward, Long > {
    
}
