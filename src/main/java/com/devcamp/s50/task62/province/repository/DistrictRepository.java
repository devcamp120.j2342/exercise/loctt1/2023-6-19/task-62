package com.devcamp.s50.task62.province.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task62.province.models.District;

public interface DistrictRepository extends JpaRepository<District, Long> {
    District getDistrictById(int id);
}
