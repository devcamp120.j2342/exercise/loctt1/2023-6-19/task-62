package com.devcamp.s50.task62.province.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task62.province.models.District;
import com.devcamp.s50.task62.province.models.Province;
import com.devcamp.s50.task62.province.models.Ward;
import com.devcamp.s50.task62.province.repository.DistrictRepository;
import com.devcamp.s50.task62.province.repository.ProvinceRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
     @Autowired
     ProvinceRepository provinceRepository;

     @GetMapping("/provinces")
     public ResponseEntity<List<Province>> getAllProvince() {
        try {
            List<Province> provinces = new ArrayList<Province>();
            provinceRepository.findAll().forEach(provinces::add);
            return new ResponseEntity<List<Province>>(provinces, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     @GetMapping("/province")
    public ResponseEntity<List<Province>> getFiveVoucher(
            @RequestParam(value = "page", defaultValue = "1") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Province> list = new ArrayList<Province>();
            provinceRepository.findAll(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/provinces/districts")
    public ResponseEntity<Set<District>> getAllDistrictOfProvince(@RequestParam int provinceId) {
        try {
            Province province = provinceRepository.getProvinceById(provinceId);
            if (province != null) {
                return new ResponseEntity<>(province.getDistricts(), HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    DistrictRepository districtRepository;

    @GetMapping("/provinces/districts/wards")
    public ResponseEntity<Set<Ward>> getAllWardOfDistrict(@RequestParam int districtId) {
        try {
            District district = districtRepository.getDistrictById(districtId);
            if (district != null){
                return new ResponseEntity<>(district.getWards(), HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
