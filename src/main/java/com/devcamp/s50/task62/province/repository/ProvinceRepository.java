package com.devcamp.s50.task62.province.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task62.province.models.Province;

public interface ProvinceRepository extends JpaRepository<Province, Long> {
    Province getProvinceById(int id);
}
